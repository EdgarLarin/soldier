package com.progectSoldier.army;

import com.progectSoldier.backpack.Backpack;
import com.progectSoldier.weapon.Gun;
import com.progectSoldier.weapon.GunMagazine;

public class Soldier {

    private Backpack backpack;
    private GunMagazine magazine;
    Gun rightHand;
    Gun leftHand;

    public Backpack getBackpack() {
        return backpack;
    }

    public void setBackpack(Backpack backpack) {
        this.backpack = backpack;
    }

    public GunMagazine getMagazine() {
        return magazine;
    }

    public void setMagazine(GunMagazine magazie) {
        this.magazine = magazie;
    }

    public Gun getRightHand() {
        return rightHand;
    }

    public void setRightHand(Gun rightHand) {
        this.rightHand = rightHand;
    }

    public Gun getLeftHand() {
        return leftHand;
    }

    public void setLeftHand(Gun leftHand) {
        this.leftHand = leftHand;
    }

    /**
     * Method for reloading magazine from soldier
     */

    public void reloadMagazine(GunMagazine magazine) {
        rightHand.reload(magazine);
    }

    /**
     * Method for change weapon from soldier -
     * Shift the weapon in soldier left hand
     * Get weapon out of the bag right hand
     * Put the gun in a backpack left hand
     * <p>
     * leftHand = rightHand;
     * rightHand = backpack;
     * backpack = leftHand;
     */
    public void changeWeapon() {
        leftHand = rightHand;
        setRightHand(backpack.getWeaponPocket());
        backpack.setWeaponPocket(leftHand);

        System.out.println(" In the right hand of a soldier " + rightHand);
    }

    /**
     * Shot method for all weapon Gun class, with checks for weapon in the hands and ammunition
     */

    public int methodOfShooting(int typeOfShooting, int longPressOfTrigger) {
        int result = 0;
        if (rightHand != null) {
            int size = rightHand.getMagazine().currentSize();
            if (size == 0) {
                GunMagazine magazine = backpack.takeMagazine();
                if (magazine != null) {
                    System.out.println("Magazine is empty reload it");
                    reloadMagazine(magazine);
                    rightHand.shootingByType(typeOfShooting, longPressOfTrigger);
                    result = 1;
                }
            } else {
                rightHand.shootingByType(typeOfShooting, longPressOfTrigger);
                result = 1;
            }
            size = rightHand.getMagazine().currentSize();
            if (size == 0) {
                GunMagazine magazine = backpack.takeMagazine();
                if (magazine != null) {
                    System.out.println("Magazine is empty reload it");
                    reloadMagazine(magazine);
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return
                "magazine = " + rightHand.getMagazine().currentSize() +
                        '}';
    }
}
