package com.progectSoldier.backpack;

import com.progectSoldier.weapon.Gun;
import com.progectSoldier.weapon.GunMagazine;

import java.util.LinkedList;

public class Backpack {

    private GunMagazine glockMagazine;
    LinkedList<GunMagazine> magazines;

    /**
     * Pocket for off hand weapon
     */

    Gun weaponPocket;

    /**
     * @param amountOfMagazine Pocket for weapon magazine
     */
    public Backpack(int amountOfMagazine) {
        magazines = new LinkedList();
    }

    public void addGlockMagazine(GunMagazine glockMagazine) {
        magazines.addLast(glockMagazine);
    }

    public GunMagazine takeMagazine() {
        return magazines.poll();
    }

    public GunMagazine getGlockMagazine() {
        return glockMagazine;
    }

    public void setGlockMagazine(GunMagazine glockMagazine) {
        this.glockMagazine = glockMagazine;
    }

    public LinkedList<GunMagazine> getMagazines() {
        return magazines;
    }

    public void setMagazines(LinkedList<GunMagazine> magazines) {
        this.magazines = magazines;
    }

    public Gun getWeaponPocket() {
        return weaponPocket;
    }

    public void setWeaponPocket(Gun weaponPocket) {
        this.weaponPocket = weaponPocket;
    }

    @Override
    public String toString() {
        return "backpack{" +
                ", currentGlockMagazine = " + magazines.size() +
                '}';
    }
}


