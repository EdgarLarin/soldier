package com.progectSoldier.weapon;

import java.util.LinkedList;

public class GunMagazine {

    private String brandName = "";
    private String type = "";
    private String caliber = "";
    private int sizeOfMagazine = 17;
    LinkedList<GunAmmunition> ammunitions;

    public GunMagazine() {
        ammunitions = new LinkedList<>();
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public int getSizeOfMagazine() {
        return sizeOfMagazine;
    }

    public void setSizeOfMagazine(int sizeOfMagazine) {
        this.sizeOfMagazine = sizeOfMagazine;
    }

    public void addAmmunition(GunAmmunition ammunition) {
        if (ammunitions.size() < sizeOfMagazine) {
            ammunitions.addLast(ammunition);
        }
    }

    public GunAmmunition cartridgeLoss() {
        return ammunitions.poll();
    }

    public int currentSize() {
        return ammunitions.size();
    }

    @Override
    public String toString() {
        return "GunMagazine{" +
                "currentAmmunitions=" + getSizeOfMagazine() +
                '}';
    }
}

