package com.progectSoldier.weapon;

public class Gun {

    public static final int ONE_SHOT_TYPE = 0;
    public static final int TRIPLE_SHOT_TYPE = 1;
    public static final int BURST_SHOT_TYPE = 2;

    private String brandName = "";
    private String type = "";
    private String aim = "";
    private GunMagazine magazine;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public GunMagazine getMagazine() {
        return magazine;
    }

    public void setMagazine(GunMagazine magazine) {
        this.magazine = magazine;
    }

    /**
     * single shooting
     */
    public void shot() {
        magazine.cartridgeLoss();
    }

    /**
     * shooting short bursts
     */
    public void tripleShot() {
        shot();
        shot();
        shot();
    }

    /**
     * burst shooting
     */
    public static int numberOfShotsBurstShooting(int longPressOnTheTrigger) {
        int rateOfFire = 1;
        int numberOfShots = longPressOnTheTrigger * rateOfFire;
        return numberOfShots;
    }

    public void burstShooting(int longPressOnTheTrigger) {
        int numberOfShots = numberOfShotsBurstShooting(longPressOnTheTrigger);
        for (int i = 0; i < numberOfShots; i++) {
            shot();
        }
    }

    /**
     * Shooting types method
     * @param shootingType
     * @param longPressOnTheTrigger
     */

    public void shootingByType(int shootingType, int longPressOnTheTrigger) {

        if (shootingType == ONE_SHOT_TYPE) {
            shot();
        }
        if (shootingType == TRIPLE_SHOT_TYPE) {
            tripleShot();
        }
        if (shootingType == BURST_SHOT_TYPE) {
            burstShooting(longPressOnTheTrigger);
        }
    }

    public void reload(GunMagazine magazine) {
        this.magazine = magazine;
    }

    @Override
    public String toString() {
        return "Gun{" +
                "brandName='" + brandName + '\'' +
                ", magazine=" + magazine.toString() +
                '}';
    }
}

