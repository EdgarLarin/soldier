package com.progectSoldier.weapon;

public class MainGun {

    public static void main(String[] args) {

        Gun glock = new Gun();

        glock.setBrandName("weapon 19 Pro");
        glock.setType("pistol");
        glock.setAim("Reflex sight");

        glock.setMagazine(chargeMagazine("weapon 19 Pro magazine", "9 × 19 mm Parabellum", "9 × 19 mm", 17));

        System.out.println(glock.toString());

//        int numberOfClick = 3;   // the number of clicks on the trigger
//        for (int i = 0; i < numberOfClick; i++) {
//            weapon.shot();
//        }
//
//        System.out.println(weapon.toString());

//        for (int i = 0; i < numberOfClick; i++) {
//            weapon.tripleShot();
//        }
//
//        System.out.println(weapon.toString());

        /**
         * The duration of pressing the trigger
         */
        int durationOfPressing = 7;
        glock.burstShooting(durationOfPressing);

        System.out.println(glock.toString());
    }

    public static GunMagazine chargeMagazine(String brandName, String type, String caliber, int size) {
        GunMagazine magazine = new GunMagazine();
        magazine.setBrandName(brandName);
        magazine.setType(type);
        magazine.setCaliber(caliber);
        magazine.setSizeOfMagazine(size);

        for (int i = 0; i < magazine.getSizeOfMagazine(); i++) {
            GunAmmunition ammunitionX = new GunAmmunition();
            ammunitionX.setBrandName(brandName);
            ammunitionX.setType(type);
            ammunitionX.setCaliber(caliber);

            magazine.addAmmunition(ammunitionX);

        }

        return magazine;
    }
}
