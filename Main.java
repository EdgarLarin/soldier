package com.progectSoldier;

import com.progectSoldier.army.Soldier;
import com.progectSoldier.backpack.Backpack;
import com.progectSoldier.weapon.Gun;
import com.progectSoldier.weapon.GunAmmunition;
import com.progectSoldier.weapon.GunMagazine;

public class Main {

    static int numberOfPockets = 12;
    static int numberOfCartridgesGlock19 = 17;
    static int numberOfCartridgesGlock17 = 12;

    public static void main(String[] args) {

        Soldier soldier = new Soldier();

        Backpack backpack = new Backpack(numberOfPockets);

        int numberOfGlock19Magazine = 5;
        int numberOfGlock17Magazine = 7;

        /*
         * Loaded magazines backpack
         */

        for (int i = 0; i < numberOfGlock19Magazine; i++) {
            backpack.addGlockMagazine(chargedGlockMagazine("glock 19 Pro magazine", "9 × 19 mm Parabellum", "9 × 19 mm", numberOfCartridgesGlock19));
        }

        for (int i = 0; i < numberOfGlock17Magazine; i++) {
            backpack.addGlockMagazine(chargedGlockMagazine("glock 17 magazine", "9 × 19 mm Parabellum", "9 × 19 mm", numberOfCartridgesGlock17));
        }

        /**
         * Add off hand weapon in backpack
         */

        backpack.setWeaponPocket(newGlock("weapon 19 Pro", "pistol", "Reflex sight"));

        /**
         * Add backpack to soldier
         */

        soldier.setBackpack(backpack);

        /**
         * Add main hand weapon in soldiers hand, take magazine from backpack and put magazine inside gun
         */

        soldier.setRightHand(newGlock("weapon 17", "pistol", "Reflex sight"));
        GunMagazine gunMagazine = soldier.getBackpack().takeMagazine();
        soldier.reloadMagazine(gunMagazine);

        /**
         * Switch weapon
         */

        soldier.changeWeapon();

        /**
         * Shot from soldiers
         */

        int numberOfClick = 6;
        for (int i = 0; i < numberOfClick; i++) {
            int pressingTrigger = soldier.methodOfShooting(Gun.TRIPLE_SHOT_TYPE, 0);
            System.out.println(soldier.toString());
            if (pressingTrigger == 0) {
                break;
            }
        }
    }

    /**
     * Creation of a new gun
     */

    public static Gun newGlock(String brandName, String type, String aim) {
        Gun mainHandGlock = new Gun();
        mainHandGlock.setBrandName(brandName);
        mainHandGlock.setType(type);
        mainHandGlock.setAim(aim);

        return mainHandGlock;
    }

    /**
     * Loaded magazine for weapon
     */

    public static GunMagazine chargedGlockMagazine(String brandName, String type, String caliber, int size) {
        GunMagazine magazine = new GunMagazine();
        magazine.setBrandName(brandName);
        magazine.setType(type);
        magazine.setCaliber(caliber);
        magazine.setSizeOfMagazine(size);

        for (int i = 0; i < magazine.getSizeOfMagazine(); i++) {
            GunAmmunition ammunitionX = new GunAmmunition();
            ammunitionX.setBrandName(brandName);
            ammunitionX.setType(type);
            ammunitionX.setCaliber(caliber);

            magazine.addAmmunition(ammunitionX);
        }
        return magazine;
    }
}

